import classes from './ActiveQuiz.css';
import AnswersLists from "./AnswersList/AnswersList";

const ActiveQuiz =  props => (
    <div className={classes.ActiveQuiz}>
        <p className={classes.Question}>
            <span>
                <b>{props.answerNumber}.</b>&nbsp;
                {props.question}
            </span>
            <small>{props.answerNumber} of {props.quizLength}</small>
        </p>
        <AnswersLists
            state={props.state}
            answers={props.answers}
            onAnswerClick={props.onAnswerClick}
        />
    </div>
);

export default ActiveQuiz